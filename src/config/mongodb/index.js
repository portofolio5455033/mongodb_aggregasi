const dbConfig = require('./db.config')

// objek modeling dari suatu dokument yang ada di mongodb
const mongoose = require('mongoose')

mongoose.Promise = global.Promise

const db = {}
db.mongoose = mongoose
db.url = dbConfig.url
db.nilai = require('./nilai.model')(mongoose)

module.exports = db