// API andpoint
module.exports = (app) => {
    const Nilai = require('../controllers/post.controller')
    const router = require('express').Router()

    router.get('/', Nilai.findAll)
    router.post('/', Nilai.create)
    router.get('/:nama', Nilai.findOne)
    router.put('/:nama', Nilai.update)
    router.delete('/:nama', Nilai.delete)

    app.use('/api/nilai', router)
}