const db = require('../index')
const Nilai = db.nilai

exports.findAll = (req, res) => {
    Nilai.find()
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(500).send({
            message: err.message || "Some error while retrieving posts."
        })
    });
}

exports.create = (req, res) => {
    const nilai = new Nilai({
        nama: req.body.nama,
        jenis_kelamin: req.body.jenis_kelamin,
        nilai: req.body.nilai,
        tanggal_lahir: req.body.tanggal_lahir,
        tempat_lahir: req.body.tempat_lahir
    })

    nilai.save()
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while create post."
        })
    });
}


// menampilkan salah satu data post berdasarkan id nya
exports.findOne = (req, res) => {
    const nama = req.params.nama
    
    Nilai.findOne({nama})
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while show post."
        })
    });
}

// membuat update data
exports.update = (req, res) => {
    const nama = req.params.nama

    Nilai.findByIdAndUpdate(nama, req.body)
    .then((result) => {
        if (!result) {
            res.status(404).send({
                message: "Post not found"
            })
        }

        res.json(
            // message: "Post was updated"
            result
        )
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while update post."
        })
    });
}

// membuat delete
exports.delete = (req, res) => {
    const nama = req.params.nama

    Nilai.findByIdAndRemove(nama)
    .then((result) => {
        if (!result) {
            res.status(404).send({
                message: "Post not found"
            })
        }

        res.send({
            message: "Post was deleted"
        })
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while delete post."
        })
    });
}
