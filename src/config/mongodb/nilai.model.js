module.exports = (mongoose) => {
    const schema = mongoose.Schema(
        {
            nama: String,
            jenis_kelamin: String,
            nilai: Number,
            tanggal_lahir: Date,
            tempat_lahir: String
        },
        { timestamps: true, collection: 'nilai' }
    )
    
    // mengubah objeck _id menjadi objek id saja
    schema.method('toJSON', function() {
        const {__v, _id, ...object} = this.toObject()
        object.id = _id
        return object
    })

    const Nilai = mongoose.model("nilai", schema)
    return Nilai
}