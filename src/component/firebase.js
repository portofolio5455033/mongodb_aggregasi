import React from 'react';
import { Typography } from '@mui/material'; 
import Box from '@mui/material/Box';
import { createTheme , ThemeProvider } from '@mui/material';
import driver from '../assets/chauffeur.png';
import Grid from '@mui/material/Grid';
import briefing from '../assets/briefing.png';
import building from '../assets/building.png';
import notes from '../assets/notes.png';
import serah from '../assets/serahterima.png';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
// bikin tombol search
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
// import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';

import { Container } from '@mui/system';

// bagian ngubah tanggal (iconnya)
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import { TextField } from '@mui/material';
// membuat table
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import NoteOutlinedIcon from '@mui/icons-material/NoteOutlined';



 
const theme = createTheme({
    palette: {
        primary: {
            main : grey[200],
        },
        // primary: lightBlue,
        // secondary: pink,
    }
});


function Menu() {
    return (
        <ThemeProvider theme={theme}>
            <Box bgcolor='#303F51'>
                    <Container maxWidth="xl">
                        <Typography sx={{bgColor: 'primary'}} color='#FFFFFF'>Jadwal Operasional Supir / Riwayat Catatan</Typography>
                    </Container>
                    <Box>
                        <Grid container spacing={1} justifyContent='center'>
                            <Button>   
                            <Grid item>
                                <Box component={'img'} src={driver} sx={{width: 50 , height: 50 , margin: 4}}/>
                                    <Typography variant='subtitle2'>Rute Operasional</Typography>
                                   
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <Box component={'img'} color='primary'src={briefing} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Briefing</Typography>
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={building} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Go/No Go Item</Typography>
                            </Grid> 
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>  
                            <Grid item>
                                <Box component={'img'} src={notes} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2' color='#ECAB55'>Riwayat Catatan</Typography>
                            </Grid>   
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />

                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={serah} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Serah Terima Dinasan</Typography>
                               
                            </Grid>  
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <WarningAmberRoundedIcon color='primary'sx={{width: 60 , height: 60 , margin: 3.5}} />
                                <Typography variant='subtitle2'>Bantuan Darurat</Typography>
                            </Grid>
                            </Button>
                        </Grid>
                    </Box>
                       
                        <Grid container justifyContent='center'>
                            {/* tombol search */}
                            <Paper component="form" sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400, marginTop: 2 }}>
                                {/* <IconButton sx={{ p: '10px' }} aria-label="menu">
                                    <MenuIcon />
                                </IconButton> */}
                                <InputBase
                                    sx={{ ml: 1, flex: 1 }}
                                    placeholder="Pencarian"
                                    inputProps={{ 'aria-label': 'Pencarian' }}
                                />
                                    <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                                        <SearchIcon/>
                                    </IconButton>

                            </Paper>
                        </Grid>

                    <Box>
                        <Typography 
                        sx={{fontFamily: 'Montserrat', marginTop: 4}} 
                        color='white'
                        align='center'
                        >RIWAYAT CATATAN OPERASIONAL</Typography> 
                    </Box>   
                    

                    <Container maxWidth="xl">
                    {/* Bagian Select Bulan */}
                    <Box display='flex' justifyContent='center' pt={2}>
                        <Box>
                            <Button variant="contained" sx={{backgroundColor: '#313541', width: 40, height: 40}}>
                            <KeyboardArrowLeftIcon fontSize="medium"/>
                            </Button>
                        </Box>
                        <Box>
                            <TextField
                              id="outlined-read-only-input"
                              defaultValue="Desember 2022"
                              InputProps={{
                                  inputProps: { readOnly: true, 
                                    style: { textAlign: "center", width: 269, height: 8 }
                                }
                              }}
                              sx={{backgroundColor: 'white'}}
                            />
                        </Box>
                        <Box>
                            <Button variant="contained" sx={{backgroundColor: '#313541', width: 40, height: 40}}>
                            <KeyboardArrowRightIcon fontSize="medium"/>
                            </Button>
                        </Box>
                    </Box>
                    <Divider sx={{borderColor: 'white', pt: 2}}/> 


                    {/* mumbuat table */}
                    <TableContainer component={Paper}>
                        <Table arial-label='simple table'>
                            <TableHead sx={{backgroundColor: '#303F51'}}>
                                <TableRow>
                                    <TableCell sx={{color: 'white'}}>TANGGAL</TableCell>
                                    <TableCell sx={{color: 'white'}} align='center'>WAKTU</TableCell>
                                    <TableCell sx={{color: 'white'}} align='left' >TRAYEK</TableCell>
                                    <TableCell sx={{color: 'white'}} align='left'>NO. BUS</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {tableData.map(row => (
                                    <TableRow 
                                        key={row.tanggal}
                                        sx={{'&:last-child td, &:last-child th' : { border: 0}}}
                                    >
                                        <TableCell align='left' width={140}>{row.tanggal}</TableCell>
                                        <TableCell align='center' width={90}>{row.waktu}</TableCell>
                                        <TableCell align='left' width={270}>{row.trayek}</TableCell>
                                        <TableCell align='left'>{row.no_bus}</TableCell>
                                        <TableCell align='center' bgColor='#28A745' sx={{width: 157}}>
                                            
                                            <Button
                                                // variant="contained"
                                                size='large'
                                                // color="success"
                                                sx={{
                                                    mr: 1, 
                                                    width: 110,
                                                    height: '100%',
                                                    "&:hover": { backgroundColor: "28A745"},
                                                    fontFamily: 'roboto',
                                                }}
                                            >
                                            <NoteOutlinedIcon  fontSize="large"/>
                                            <Typography sx={{ ml: 1 , fontFamily: 'roboto', fontSize: 10, textAlign: 'left' }}>      
                                            Lihat
                                            Catatan
                                            </Typography>
                                            </Button> 
                                
                                        </TableCell>
                                    </TableRow>
                              
                                ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>

                    </Container>                   
            </Box>
        </ThemeProvider>
    );
}

export default Menu;

const tableData = [{
    "tanggal": "Jumat, 30 Des 2022",
    "waktu": "06:00 WIB",
    "trayek": "TERM. PONOROGO - KALIDERES",
    "no_bus": "BM007",
  }, {
    "tanggal": "Rabu, 28 Des 2022",
    "waktu": "06:20 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM007",
  }, {
    "tanggal": "Selasa, 27 Des 2022",
    "waktu": "07:00 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM023",
  }]